import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import devConfig from '../dev.app.conf.json';
import prodConfig from '../prod.app.conf.json';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  let origin = process.env.NODE_ENV == 'production' ? prodConfig.corsPolicy.origin : devConfig.corsPolicy.origin;
  app.enableCors({ origin });
  app.useGlobalPipes(new ValidationPipe({ whitelist: true, transform: true }));
  await app.listen(3000);
}
bootstrap();
