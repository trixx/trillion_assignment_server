
import { Type } from 'class-transformer';
import { IsInt, Max, Min } from 'class-validator';

export class PaginationQueryDto {
    @IsInt()
    @Type(() => Number)
    public page: number = 0; // default value to 0

    @IsInt()
    @Type(() => Number)
    @Min(3)
    @Max(15)
    public resultsPerPage: number = 5 // default value to 5
}