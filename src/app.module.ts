import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ArticlesModule } from './articles/articles.module';
import { Article } from './articles/article.entity';
import devConfig from '../dev.app.conf.json';
import prodConfig from '../prod.app.conf.json';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "mysql",
      host: process.env.NODE_ENV == 'production' ? prodConfig.database.host : devConfig.database.host,
      port: process.env.NODE_ENV == 'production' ? prodConfig.database.port : devConfig.database.port,
      username: process.env.NODE_ENV == 'production' ? prodConfig.database.username : devConfig.database.username,
      password: process.env.NODE_ENV == 'production' ? prodConfig.database.password : devConfig.database.password,
      database: process.env.NODE_ENV == 'production' ? prodConfig.database.database : devConfig.database.database,
      entities: [Article],
      synchronize: false,
    }),
    ArticlesModule,
  ]
})
export class AppModule { }
