
import { Controller, Res, Get, Param, ClassSerializerInterceptor, UseInterceptors, Query } from '@nestjs/common';
import { ArticlesService } from './articles.service';
import { Article } from './article.entity';
import { Observable, of } from 'rxjs';
import { join } from 'path';
import { PaginationQueryDto } from '../dto/paginationQueryDto';
import devConfig from '../../dev.app.conf.json';
import prodConfig from '../../prod.app.conf.json';


@Controller('articles')
export class ArticlesController {

    constructor(private articlesService: ArticlesService) { }

    @UseInterceptors(ClassSerializerInterceptor)
    @Get()
    getArticles(@Query() queryParams: PaginationQueryDto): Promise<[Article[], number]> {
        return this.articlesService.find(queryParams.page, queryParams.resultsPerPage);
    }

    @UseInterceptors(ClassSerializerInterceptor)
    @Get('/similar/:id',)
    getSimilarArticles(@Param('id') id: number, @Query() queryParams: PaginationQueryDto): Promise<Article[]> {
        return this.articlesService.findSimilarArticles(id, queryParams.page, queryParams.resultsPerPage);
    }

    @Get('/:slug',)
    getBySlug(@Param('slug') slug: string): Promise<Article> {
        return this.articlesService.findBySlug(slug);
    }

    @Get('/image/:imagename')
    findArticleImage(@Param('imagename') imagename: string, @Res() res): Observable<Object> {
        let imageDirectory = process.env.NODE_ENV == 'production' ? prodConfig.files.imagesDirectory : "uploads/images/";
        return of(res.sendFile(join(process.cwd(), imageDirectory + imagename)));
    }

}