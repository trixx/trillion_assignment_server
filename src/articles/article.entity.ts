import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, JoinTable, } from 'typeorm';
import { Exclude } from 'class-transformer';

@Entity()
export class Article {

    @PrimaryGeneratedColumn({ name: 'id' })
    id: number;

    @Column({
        name: 'name',
        nullable: false
    })
    name: string;

    @Column({
        name: 'slug',
        nullable: false
    })
    slug: string;

    @Column({
        name: 'image_name',
        nullable: false
    })
    imageName: string;

    @Column({
        name: 'content',
        nullable: false
    })
    @Exclude() //will be excluded from returned json oject for controllers that use ClassSerializerInterceptor 
    content: string;

    @Column({
        name: 'creation_date',
        nullable: false
    })
    creationDate: Date;

    @ManyToMany(() => Article)
    @JoinTable({
        name: 'similar_article',
        joinColumn: {
            name: 'fk_article_id',
            referencedColumnName: 'id',
        },
        inverseJoinColumn: {
            name: 'fk_similar_article_id',
            referencedColumnName: 'id',
        },
    })
    similarArticles: Article[];

}
