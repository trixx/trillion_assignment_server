import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, EntityManager } from 'typeorm';
import { Article } from './article.entity';

@Injectable()
export class ArticlesService {

    constructor(@InjectRepository(Article) private articlesRepository: Repository<Article>, private entityManager: EntityManager) { }

    async find(page: number, resultsPerPage: number): Promise<[Article[], number]> {
        let offset = page * resultsPerPage;
        return await this.articlesRepository.findAndCount({ take: resultsPerPage, skip: offset });
    }

    async findBySlug(slug: string): Promise<Article> {
        let article: Article;
        article = await this.articlesRepository.findOne({
            where: { slug }
        });
        return article;
    }

    async findSimilarArticles(id: number, page: number, resultsPerPage: number): Promise<Article[]> {

        let offset = page * resultsPerPage;
        // We want the bidirectional similar articles and we would use createQueryBuilder but rightJoin is not
        // supported from typeOrm so we have to use native query if we want the bidirectional similar articles
        return await this.entityManager.query(
            `SELECT artcl.id, artcl.name, artcl.slug, artcl.image_name imageName, artcl.creation_date creationDate
            FROM article artcl
            WHERE artcl.id <> ?
                AND EXISTS(
                SELECT 1
                FROM similar_article smlartcl
                WHERE (smlartcl.fk_article_id = ? AND smlartcl.fk_similar_article_id = artcl.id )
                OR (smlartcl.fk_similar_article_id = ? AND smlartcl.fk_article_id = artcl.id )
                ) LIMIT ?,? ;`, [id, id, id, offset, resultsPerPage]
        );
    }

}
